<?php

/**
 * Implements hook_views_data()
 */
function views_sort_mvf_views_data() {
  return array(
    'views' => array(
      'sort_multivalue_field' => array(
        'title' => t('Sort by multi-value field'),
        'group' => t('Global'),
        'sort'  => array(
          'handler' => 'views_handler_sort_multivalue_field',
        ),
      ),
    ),
  );
}
