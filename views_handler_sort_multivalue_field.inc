<?php

class views_handler_sort_multivalue_field extends views_handler_sort {
  function option_definition() {
    $options = parent::option_definition();
    
    $options['field_name'] = array('default' => '');
    $options['field_value'] = array('default' => '');
    
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['field_name'] = array(
      '#type' => 'textfield',
      '#title' => 'Field name',
      '#default_value' => $this->options['field_name'],
    );
    
    $form['field_value'] = array(
      '#type' => 'textfield',
      '#title' => 'Field value',
      '#default_value' => $this->options['field_value'],
    );
  }

  function query() {
    $escaped_field_name = db_escape_field($this->options['field_name']);
    $escaped_field_value = db_escape_field($this->options['field_value']);
    $expression = "
      '{$escaped_field_value}' IN (
        SELECT {$escaped_field_name}_value
        FROM field_data_{$escaped_field_name} fd
        WHERE fd.entity_id = node.nid
      )
    ";
    $this->query->add_orderby(NULL, $expression, $this->options['order'], $escaped_field_name . '_has_value_' . $escaped_field_value);
  }
}
